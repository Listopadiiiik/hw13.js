// Теоретичні питання

// 1. settimeout викликає функцію 1 раз, а setinterval повторює виконання за заданий період.

// 2. миттєво не спрацює, якщо до settimeout є код, який ще виконується.
// Тобто вона викликається як тільки настає її час.

// 3. без clearinterval ми просто даємо функціїї повторюватись, навіть якщо вона не потрібна.

// Завдання

// P.s. не реалізував кнопку continue ((

let img = Array.from(document.querySelectorAll('.image-to-show'));
let startButton = document.querySelector('#start-btn');
let stopButton = document.querySelector('#stop-btn');
let resumeButton = document.querySelector('#resume-btn');

let slide = function(){
let show0 = function (){
    img[0].classList.add('active');
}
let hide0 = function (){
    img[0].classList.remove('active');
}
show0()
let hide = window.setTimeout(hide0, 3000);

let show1 = function (){
    img[1].classList.add('active');
    true;
}
let hide1 = function(){
    img[1].classList.remove('active');
}

let hide4 = window.setTimeout(show1, 3000);
let show4 = window.setTimeout(hide1, 6000);

let show2 = function(){
    img[2].classList.add('active');
}
let hide2 = function(){
    img[2].classList.remove('active');
}

let show5 = window.setTimeout(show2, 6000);
let hide5 = window.setTimeout(hide2, 9000);

let show3 = function(){
    img[3].classList.add('active');
}
let hide3 = function(){
    img[3].classList.remove('active');
}

let show6 = window.setTimeout(show3, 9000);
let hide6 = window.setTimeout(hide3, 12000);

function stopper(){
    stopButton.setAttribute('type', 'disabled');
    clearTimeout(hide);
    clearTimeout(hide4);
    clearTimeout(show4);
    clearTimeout(hide5);
    clearTimeout(show5);
    clearTimeout(hide6);
    clearTimeout(show6);
    }
    stopButton.addEventListener('click', stopper);
    resumeButton.removeEventListener('click', stopper);
}
startButton.addEventListener('click', function(){
    slide();
    startButton.setAttribute('type', 'disabled');
});